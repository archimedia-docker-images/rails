#Immagini per Rails

##Changelog

###0.0.1
* Immagine base

###0.0.2
* Aggiunta Timezone Europe/Rome

###0.0.3
* upgrade debian to strech
* optimized variabled image generation

#Compilare l'immagine

VERSION=0.0.2


##Build locale con variabili
export IMAGE_TAG=0.0.2
export BASE_DOCKER_IMAGE=ruby:2.4-stretch
export BASE_IMAGE_NAME=rails/2.5
export TARGET_BUILD='mysql'

docker build --pull -t "$BASE_IMAGE_NAME/$TARGET_BUILD:$IMAGE_TAG"  --build-arg BASE_DOCKER_IMAGE=$BASE_DOCKER_IMAGE ./$TARGET_BUILD/.
